using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exo
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] tableau_de_valeur = new int[10];
            int increment, valeur_tableau, saisie_recherche, position_valeur_recherche=-1;
            string valeur_tableau_as_string, saisie_recherche_as_string,phrase_boucle,phrase_conclusion;


            //Définissons les valeurs du tableau

            for (increment=0;increment<=9;increment++) 
            {
                phrase_boucle = String.Format("Saisir la valeur de {0} dans le tableau : ", increment + 1);
                Console.WriteLine(phrase_boucle);
                valeur_tableau_as_string = Console.ReadLine();
                int.TryParse(valeur_tableau_as_string, out valeur_tableau);
                tableau_de_valeur[increment] = valeur_tableau;
            }

            //Saisie de la valeur recherchée

            Console.WriteLine("Saisissez la valeur recherchée : ");
            saisie_recherche_as_string = Console.ReadLine();
            int.TryParse(saisie_recherche_as_string, out saisie_recherche);

            //Recherchons la place de la valeur saisie_recherche, si elle est dans le tableau

            int i = 0;

            do
            {
                 if(tableau_de_valeur[i]==saisie_recherche)
                {
                    position_valeur_recherche = i + 1;      //modificication              
                }
                i++;
            } while (i < tableau_de_valeur.Length && position_valeur_recherche == -1) ;
                

            //on donne le résultat de la recherche a l'utilisateur
            if(position_valeur_recherche !=-1)
            {
                phrase_conclusion = String.Format("La valeur {0} se trouve a l'emplacement : {1} ", saisie_recherche, position_valeur_recherche);
                Console.WriteLine(phrase_conclusion);
            }
            else
            {
                phrase_conclusion = String.Format("La valeur {0} n'est pas dans le tableau, sorry :) ", saisie_recherche);
                Console.WriteLine(phrase_conclusion);
            }

            Console.ReadKey();
        }
    }
}
